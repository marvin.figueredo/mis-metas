import 'dart:async';

import 'package:metas/src/providers/DBProvider.dart';

class SyncBloc {

  static final SyncBloc _singleton = new SyncBloc._internal();

  factory SyncBloc() {
    return _singleton;
  }

  SyncBloc._internal() {
    obtenerCantidadSincronizar();
  }

  final _syncController = StreamController<int>.broadcast();

  Stream<int> get syncStream => _syncController.stream;

  dispose() {
    _syncController?.close();
  }

  sincronizarConServidor() async {
    await DBProvider.db.sincronizarConServidor();
    await obtenerCantidadSincronizar();
  }

  obtenerCantidadSincronizar() async {
    _syncController.sink.add( await DBProvider.db.getPendientesSicronizar() );
  }
}
