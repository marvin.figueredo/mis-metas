import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:metas/src/bloc/metas_bloc.dart';
import 'package:metas/src/bloc/provider.dart';
import 'package:metas/src/bloc/sincronizacion_bloc.dart';
import 'package:metas/src/models/metas_models.dart';
import 'package:metas/src/search/search_delegate.dart';
import 'package:metas/src/utils/funciones_utiles.dart';
import 'package:metas/src/widgets/metas_list_widgt.dart';
import 'package:progress_dialog/progress_dialog.dart';

class HomePage extends StatefulWidget {
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() { 
    super.initState();
  }

  @override
  Widget build(BuildContext context)  {
    final metasBloc = Provider.of(context);
    final syncBloc = SyncBloc();

    metasBloc.obtenerMetas();

    return Scaffold(
      appBar: _titulo(context, syncBloc),
      body: _crearListado(context, metasBloc),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: Icon(Icons.add, color: Colors.white),
        onPressed: (){

          final metaArgument = Meta();  
          metaArgument.id = '';
          metaArgument.parentid = '';

          Navigator.pushNamed(context, 'add', arguments: metaArgument);
        },
      ),
    );
  }

  Widget _crearListado(BuildContext context, MetasBloc bloc){
    return StreamBuilder<List<Meta>>(
      stream: bloc.metasStream,
      builder: (BuildContext context, AsyncSnapshot<List<Meta>> snapshot){

        if ( !snapshot.hasData ) {
          return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.blueGrey)));
        }

        final _listMetas = snapshot.data.where((c) => c.parentid == '').toList();

        return Container(
          padding: EdgeInsets.only(top: 5.0),
          child: MetasListWidget(
              listMetas: _listMetas
            ),
        );
      },
    );
  }

  Widget _titulo(BuildContext context, SyncBloc syncBloc){


    return AppBar(
      title: Text('Mis Metas', style: TextStyle(color: Colors.white)),
      flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[colorHeader,Colors.blue]
              ) 
      )),
      iconTheme: new IconThemeData(color: Colors.white),
      actions: <Widget>[
        _iconSync(context, syncBloc),
        IconButton(
          icon: Icon(Icons.search),
          onPressed: () {
            showSearch(context: context, delegate: DataSearch());
          },
        )
      ],
    );
  }

  Widget _iconSync(BuildContext context, SyncBloc syncBloc) {
    return StreamBuilder<int>(
                  stream: syncBloc.syncStream,
                  builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                    if(snapshot.hasData && snapshot.data > 0){
                      return Badge(
                        position: BadgePosition.topLeft(top: 2.0, left: 2.5),
                        badgeContent: Text(snapshot.data.toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                        child: IconButton(
                          icon: Icon(Icons.sync, color: Colors.white),
                          onPressed: () async {
                            final ProgressDialog pr = ProgressDialog(context, type: ProgressDialogType.Normal, isDismissible: true, showLogs: false);
                            pr.style(message: "Sincronizando...");
                            await pr.show();

                            await syncBloc.sincronizarConServidor();

                            pr.hide();

                          },
                        )
                      );
                    }
                    else{
                      return Container();
                    }
                  });
  }
}